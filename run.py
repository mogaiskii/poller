import os
from app import Application
from app.api.routes import *


if __name__ == "__main__":
    # port = int(os.environ.get("PORT", 5000))
    Application.run(host='0.0.0.0', port=Application.config['PORT'])
