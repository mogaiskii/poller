import requests

HOST = 'http://localhost:5000'
MAIN_USER_ID = 1
MAIN_USER_CODE = 'Boss_Pass'


print('POST /auth/')
request_auth = requests.post(HOST + '/auth/', json={
    "code": MAIN_USER_CODE
})

response = request_auth.json()

AUTH_TOKEN = response['auth_token']
HEADERS = {'Authorization': AUTH_TOKEN}


print('GET /auth/')
request_check_auth = requests.get(HOST + '/auth/', headers=HEADERS)

assert request_check_auth.status_code == 200




print('\n\nAUTH OK\n\n')




print('GET /superfeatures/am_i_bad/')
request_am_i_bad = requests.get(HOST + '/superfeatures/am_i_bad/', headers=HEADERS)

assert request_am_i_bad.status_code == 200

assert request_am_i_bad.json()['no__i_am_boss_ass_bitch'] == True

assert request_am_i_bad.json()['i_am_bad'] == False




print('\n\nSUPERFEATURES OK\n\n')




print('POST /boss_route/info/')
request_post_info = requests.post(HOST + '/boss_route/info/', headers=HEADERS, json={
    "title": "Test Title",
    "text": "Test Text"
})

assert request_post_info.status_code == 200

response = request_post_info.json()

assert response['title'] == "Test Title"
assert response['text'] == "Test Text"

print('GET /info/')
request_get_info = requests.get(HOST + '/info/', headers=HEADERS)
response = request_get_info.json()

assert response['title'] == "Test Title"
assert response['text'] == "Test Text"




print('\n\INFO OK')


poll_creation_data = {
    "poll_name": "Go poll!",
    "due_to": "2020-09-01T00:01:01",
    "active": True,
    "available_votes_actions": 1,
    "poll_structure": [
        {
            "row_name": "Go poll!",
            "row_available_votes_actions": 1,
            "type": "variant",
            "variants": [
                {
                    "title": "Variant 1",
                    "description": "Variant 1 description"
                },
                {
                    "title": "Variant 2",
                    "description": "Variant 2 description"
                }
            ]
        },
        {
            "row_name": "Go range!",
            "type": "range",
            "range": {
                "min_value": 1,
                "max_value": 100
            }
        }
    ],
    "allowed_users": [] # TODO: [MAIN_USER_ID]
}

print('POST /boss_route/polls/')
request_create_poll = requests.post(HOST + '/boss_route/polls/', headers=HEADERS, json=poll_creation_data)

assert request_create_poll.status_code == 200
response = request_create_poll.json()

VOTE_ID = response['id']


def validate_create_poll(req_data, res_data):
    if isinstance(req_data, dict):
        for k,v in req_data.items():
            if isinstance(v, dict):
                validate_create_poll(req_data[k], res_data[k])
            elif isinstance(v, list):
                if res_data.get(k) is None:
                    print(k, res_data)
                if len(req_data[k]) != len(res_data[k]):
                    print(k)
                assert len(req_data[k]) == len(res_data[k])
                for i in range(len(v)):
                    validate_create_poll(req_data[k][i], res_data[k][i])
            else:
                if req_data[k] != res_data[k]:
                    print(k)
                assert req_data[k] == res_data[k]
    else:
        if req_data != res_data:
            print(req_data, res_data)
        assert req_data == res_data

validate_create_poll(poll_creation_data, response)



print('GET /boss_route/polls/' + str(VOTE_ID))
request_get_poll_admin = requests.get(HOST + '/boss_route/polls/' + str(VOTE_ID), headers=HEADERS)

assert request_get_poll_admin.status_code == 200
response = request_get_poll_admin.json()

validate_create_poll(poll_creation_data, response)



poll_update_data_json = {
    "poll_name": "Go poll1!",
    "due_to": "2020-09-01T00:01:01",
    "active": True,
    "available_votes_actions": 1,
    "poll_structure": [
        {
            "id": response['poll_structure'][0]['id'],
            "row_name": "Go 1",
            "row_available_votes_actions": 2,
            "type": "variant",
            "variants": [
                {
                    "title": "Variant 12",
                    "description": "Variant 12 description"
                },
                {
                    "title": "Variant 22",
                    "description": "Variant 22 description"
                }
            ]
        },
        {
            "row_name": "Go range 2!",
            "type": "range",
            "range": {
                "min_value": 2,
                "max_value": 99
            }
        }
    ],
    'allowed_users': []
}
print('POST /boss_route/polls/' + str(VOTE_ID))
request_update_poll_admin = requests.post(HOST + '/boss_route/polls/' + str(VOTE_ID), headers=HEADERS, json=poll_update_data_json)

assert request_update_poll_admin.status_code == 200

response = request_update_poll_admin.json()

validate_create_poll(poll_update_data_json, response)


print('GET /boss_route/polls/' + str(VOTE_ID))
request_get_poll_admin = requests.get(HOST + '/boss_route/polls/' + str(VOTE_ID), headers=HEADERS)

assert request_get_poll_admin.status_code == 200
response = request_get_poll_admin.json()

validate_create_poll(poll_update_data_json, response)




print('\n\nPOLLS - ADMINS HALF\n\n')




print('POST /boss_route/users/')
request_create_user = requests.post(HOST + '/boss_route/users/', headers=HEADERS, json={
    "name": "Sasha",
    "code": "1234",
    "vk_id": "id123456",
    "is_staff": False,
    "bad_guy": False,
    "blocked": False,
    "phone": "+79000000000",
    "allowed_votes": [
        VOTE_ID
    ],
    "friend_of": [
        MAIN_USER_ID
    ]
})

assert request_create_user.status_code == 200
response = request_create_user.json()

assert response['name'] == 'Sasha'
assert response['code'] == '1234'
assert response['vk_id'] == 'id123456'
assert response['is_staff'] == False
assert response['bad_guy'] == False
assert response['blocked'] == False
assert response['phone'] == "+79000000000"
assert len(response['allowed_votes']) == 1
assert response['allowed_votes'][0] == VOTE_ID
assert len(response['friend_of']) == 1
assert response['friend_of'][0] == MAIN_USER_ID

ADD_USER_ID = response['id']


print('GET /boss_route/users/')
request_get_users = requests.get(HOST + '/boss_route/users/', headers=HEADERS)
assert request_get_users.status_code == 200

response = request_get_users.json()

assert len(response['users']) == 2

for user in response['users']:
    if user['id'] == ADD_USER_ID:
        response = user
        break

assert response['name'] == 'Sasha'
assert response['code'] == '1234'
assert response['vk_id'] == 'id123456'
assert response['is_staff'] == False
assert response['bad_guy'] == False
assert response['blocked'] == False
assert response['phone'] == "+79000000000"
assert len(response['allowed_votes']) == 1
assert response['allowed_votes'][0] == VOTE_ID
assert len(response['friend_of']) == 1
assert response['friend_of'][0] == MAIN_USER_ID


print('POST /boss_route/users/search/')
request_search_users = requests.post(HOST + '/boss_route/users/search/', headers=HEADERS, json={
    "ids": [ADD_USER_ID],
    "name": "Sasha",
    "phone": "+79000000000",
    "vk_id": 'id123456'
})

assert request_search_users.status_code == 200
response = request_search_users.json()

assert len(response['users']) == 1
response = response['users'][0]

assert response['name'] == 'Sasha'
assert response['code'] == '1234'
assert response['vk_id'] == 'id123456'
assert response['is_staff'] == False
assert response['bad_guy'] == False
assert response['blocked'] == False
assert response['phone'] == "+79000000000"
assert len(response['allowed_votes']) == 1
assert response['allowed_votes'][0] == VOTE_ID
assert len(response['friend_of']) == 1
assert response['friend_of'][0] == MAIN_USER_ID


print('POST /boss_route/users/'+ str(ADD_USER_ID))
request_user_update = requests.post(HOST + '/boss_route/users/' + str(ADD_USER_ID), headers=HEADERS, json={
    "vk_id": "id1",
    "code": '7128',
    "name": "Alex",
    "is_staff": True,
    "bad_guy": True,
    "blocked": True,
    "phone": "+1",
    "allowed_votes": [],
    "friend_of": []
})

print('GET /boss_route/users/'+ str(ADD_USER_ID))
request_user_card = requests.get(HOST + '/boss_route/users/' + str(ADD_USER_ID), headers=HEADERS)
assert request_user_card.status_code == 200

response = request_user_card.json()

assert response['name'] == 'Alex'
assert response['code'] == '7128'
assert response['vk_id'] == 'id1'
assert response['is_staff'] == True
assert response['bad_guy'] == True
assert response['blocked'] == True
assert response['phone'] == "+1"
assert len(response['allowed_votes']) == 0
assert len(response['friend_of']) == 0



print('POST /boss_route/users/' + str(ADD_USER_ID))
request_user_update = requests.post(HOST + '/boss_route/users/' + str(ADD_USER_ID), headers=HEADERS, json={
    "vk_id": "id1234",
    "code": '1234',
    "name": "Sasha",
    "is_staff": False,
    "bad_guy": False,
    "blocked": False,
    "phone": "+79000000000",
    "allowed_votes": [VOTE_ID],
    "friend_of": [MAIN_USER_ID]
})

assert request_user_update.status_code == 200

response = request_user_update.json()

print(response['name'], 'Sasha')
assert response['name'] == 'Sasha'
assert response['code'] == '1234'
assert response['vk_id'] == 'id1234'
assert response['is_staff'] == False
assert response['bad_guy'] == False
assert response['blocked'] == False
assert response['phone'] == "+79000000000"
assert len(response['allowed_votes']) == 1
assert response['allowed_votes'][0] == VOTE_ID
assert len(response['friend_of']) == 1
assert response['friend_of'][0] == MAIN_USER_ID




print('\n\nUSERS OK\n\n')





print('POST /auth/')
request_auth = requests.post(HOST + '/auth/', json={
    "code": '1234'
})

response = request_auth.json()

NEW_AUTH_TOKEN = response['auth_token']
NEW_HEADERS = {'Authorization': NEW_AUTH_TOKEN}
print('\nToken: ', NEW_AUTH_TOKEN)
print()


print('GET /auth/')
request_check_auth = requests.get(HOST + '/auth/', headers=NEW_HEADERS)

assert request_check_auth.status_code == 200




print('\n\nAUTH OK\n\n')




print('GET /superfeatures/am_i_bad/')
request_am_i_bad = requests.get(HOST + '/superfeatures/am_i_bad/', headers=NEW_HEADERS)

assert request_am_i_bad.status_code == 200

assert request_am_i_bad.json().get('no__i_am_boss_ass_bitch') is None

assert request_am_i_bad.json()['i_am_bad'] == False



print('\n\nNEW USERS SUPERFEATURES OK\n\n')






print('GET /polls/')
request_poll_list = requests.get(HOST + '/polls/', headers=HEADERS)

assert request_poll_list.status_code == 200

response = request_poll_list.json()
assert len(response['polls']) == 1

assert response['polls'][0]['poll_name'] == poll_update_data_json['poll_name']
assert response['polls'][0]['due_to'] == poll_update_data_json['due_to']
assert response['polls'][0]['active'] == poll_update_data_json['active']
assert response['polls'][0]['available_votes_actions'] == poll_update_data_json['available_votes_actions']




print('GET /polls/')
request_poll_list = requests.get(HOST + '/polls/', headers=NEW_HEADERS)

assert request_poll_list.status_code == 200

response = request_poll_list.json()
assert len(response['polls']) == 1

assert response['polls'][0]['poll_name'] == poll_update_data_json['poll_name']
assert response['polls'][0]['due_to'] == poll_update_data_json['due_to']
assert response['polls'][0]['active'] == poll_update_data_json['active']
assert response['polls'][0]['available_votes_actions'] == poll_update_data_json['available_votes_actions']



print('GET /polls/' + str(VOTE_ID))
request_poll_list = requests.get(HOST + '/polls/' + str(VOTE_ID), headers=NEW_HEADERS)

assert request_poll_list.status_code == 200

response = request_poll_list.json()

poll_get_data_json = {
    "poll_name": "Go poll1!",
    "due_to": "2020-09-01T00:01:01",
    "active": True,
    "available_votes_actions": 1,
    "poll_structure": [
        {
            "id": poll_update_data_json['poll_structure'][0]['id'],
            "row_name": "Go 1",
            "row_available_votes_actions": 2,
            "type": "variant",
            "variants": [
                {
                    "title": "Variant 12",
                    "description": "Variant 12 description"
                },
                {
                    "title": "Variant 22",
                    "description": "Variant 22 description"
                }
            ]
        },
        {
            "row_name": "Go range 2!",
            "type": "range",
            "range": {
                "min_value": 2,
                "max_value": 99
            }
        }
    ]
}
validate_create_poll(poll_get_data_json, response)




print('POST /polls/' + str(VOTE_ID))
print('poll_var_id:', response['poll_structure'][0]["variants"][1]["id"])
request_poll_vote = requests.post(HOST + '/polls/' + str(VOTE_ID), headers=NEW_HEADERS, json={
    'poll_structure': [
        {
            "id": response['poll_structure'][0]['id'],
            "variants": [
                {
                    "id": response['poll_structure'][0]["variants"][1]["id"],
                    "voted_by_user": 1
                }
            ]
        },
        {
            "id": response['poll_structure'][1]['id'],
            "range": {
                "value": 50
            }
        }
    ]
})


assert request_poll_vote.status_code == 200

response = request_poll_vote.json()

print(response['poll_structure'][0]['variants'][0])
assert response['poll_structure'][0]['variants'][0]['voted_by_user'] == 1
assert response['poll_structure'][0]['variants'][1]['voted_by_user'] == 0
assert response['poll_structure'][1]['range']['value'] == 50



print('GET /boss_route/polls/' + str(VOTE_ID))
request_get_poll_admin = requests.get(HOST + '/boss_route/polls/' + str(VOTE_ID), headers=HEADERS)

assert request_get_poll_admin.status_code == 200
response = request_get_poll_admin.json()

assert len(response['poll_structure'][0]['variants'][0]['voted_list']) == 1
assert response['poll_structure'][0]['variants'][0]['num_votes'] == 1
assert response['poll_structure'][0]['variants'][0]['voted_list'][0] == ADD_USER_ID
assert len(response['poll_structure'][0]['variants'][1]['voted_list']) == 1
assert len(response['poll_structure'][1]['range']['votes']) == 1
assert response['poll_structure'][1]['range']['votes'][0]['value'] == 50

