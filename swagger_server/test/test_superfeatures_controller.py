# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.response_am_i_bad import ResponseAmIBad  # noqa: E501
from swagger_server.test import BaseTestCase


class TestSuperfeaturesController(BaseTestCase):
    """SuperfeaturesController integration test stubs"""

    def test_am_i_bad(self):
        """Test case for am_i_bad

        
        """
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/superfeatures/am_i_bad/',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
