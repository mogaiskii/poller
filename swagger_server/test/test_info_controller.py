# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.request_info_set_admin import RequestInfoSetAdmin  # noqa: E501
from swagger_server.models.response_info import ResponseInfo  # noqa: E501
from swagger_server.test import BaseTestCase


class TestInfoController(BaseTestCase):
    """InfoController integration test stubs"""

    def test_get_info(self):
        """Test case for get_info

        
        """
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/info/',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_set_info_admin(self):
        """Test case for set_info_admin

        
        """
        body = RequestInfoSetAdmin()
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/boss_routes/info/',
            method='POST',
            data=json.dumps(body),
            headers=headers,
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
