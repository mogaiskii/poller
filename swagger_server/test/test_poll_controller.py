# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.request_poll_card_admin_create_or_update import RequestPollCardAdminCreateOrUpdate  # noqa: E501
from swagger_server.models.request_poll_vote import RequestPollVote  # noqa: E501
from swagger_server.models.response_poll_card import ResponsePollCard  # noqa: E501
from swagger_server.models.response_poll_card_admin import ResponsePollCardAdmin  # noqa: E501
from swagger_server.models.response_polls_list import ResponsePollsList  # noqa: E501
from swagger_server.test import BaseTestCase


class TestPollController(BaseTestCase):
    """PollController integration test stubs"""

    def test_poll_card(self):
        """Test case for poll_card

        
        """
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/polls/{pollId}/'.format(poll_id=56),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_poll_card_admin(self):
        """Test case for poll_card_admin

        
        """
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/boss_route/polls/{pollId}'.format(poll_id=56),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_poll_card_admin_create(self):
        """Test case for poll_card_admin_create

        
        """
        body = RequestPollCardAdminCreateOrUpdate()
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/boss_route/polls/',
            method='POST',
            data=json.dumps(body),
            headers=headers,
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_poll_card_admin_update(self):
        """Test case for poll_card_admin_update

        
        """
        body = RequestPollCardAdminCreateOrUpdate()
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/boss_route/polls/{pollId}'.format(poll_id=56),
            method='POST',
            data=json.dumps(body),
            headers=headers,
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_poll_vote(self):
        """Test case for poll_vote

        
        """
        body = RequestPollVote()
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/polls/{pollId}/'.format(poll_id=56),
            method='POST',
            data=json.dumps(body),
            headers=headers,
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_polls_list(self):
        """Test case for polls_list

        
        """
        query_string = [('limit', 1),
                        ('offset', 1)]
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/polls/',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
