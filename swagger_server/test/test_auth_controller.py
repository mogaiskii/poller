# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.request_auth import RequestAuth  # noqa: E501
from swagger_server.models.response_auth import ResponseAuth  # noqa: E501
from swagger_server.models.success import Success  # noqa: E501
from swagger_server.test import BaseTestCase


class TestAuthController(BaseTestCase):
    """AuthController integration test stubs"""

    def test_auth_check(self):
        """Test case for auth_check

        
        """
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/auth/',
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_auth_request(self):
        """Test case for auth_request

        
        """
        body = RequestAuth()
        response = self.client.open(
            '/auth/',
            method='POST',
            data=json.dumps(body),
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
