# coding: utf-8

from __future__ import absolute_import

from flask import json
from six import BytesIO

from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.request_info_set_admin import RequestInfoSetAdmin  # noqa: E501
from swagger_server.models.request_poll_card_admin_create_or_update import RequestPollCardAdminCreateOrUpdate  # noqa: E501
from swagger_server.models.request_user_create_or_update_admin import RequestUserCreateOrUpdateAdmin  # noqa: E501
from swagger_server.models.request_user_search_admin import RequestUserSearchAdmin  # noqa: E501
from swagger_server.models.response_info import ResponseInfo  # noqa: E501
from swagger_server.models.response_poll_card_admin import ResponsePollCardAdmin  # noqa: E501
from swagger_server.models.response_user_admin import ResponseUserAdmin  # noqa: E501
from swagger_server.models.response_users_list_admin import ResponseUsersListAdmin  # noqa: E501
from swagger_server.test import BaseTestCase


class TestAdminsController(BaseTestCase):
    """AdminsController integration test stubs"""

    def test_admin_users_card(self):
        """Test case for admin_users_card

        
        """
        query_string = [('limit', 1),
                        ('offset', 1)]
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/boss_route/users/{userId}'.format(user_id=56),
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_admin_users_create(self):
        """Test case for admin_users_create

        
        """
        body = RequestUserCreateOrUpdateAdmin()
        query_string = [('limit', 1),
                        ('offset', 1)]
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/boss_route/users/',
            method='POST',
            data=json.dumps(body),
            headers=headers,
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_admin_users_list(self):
        """Test case for admin_users_list

        
        """
        query_string = [('limit', 1),
                        ('offset', 1)]
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/boss_route/users/',
            method='GET',
            headers=headers,
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_admin_users_search(self):
        """Test case for admin_users_search

        
        """
        body = RequestUserSearchAdmin()
        query_string = [('limit', 1),
                        ('offset', 1)]
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/boss_route/users/search/',
            method='POST',
            data=json.dumps(body),
            headers=headers,
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_admin_users_update(self):
        """Test case for admin_users_update

        
        """
        body = RequestUserCreateOrUpdateAdmin()
        query_string = [('limit', 1),
                        ('offset', 1)]
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/boss_route/users/{userId}'.format(user_id=56),
            method='POST',
            data=json.dumps(body),
            headers=headers,
            content_type='application/json',
            query_string=query_string)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_poll_card_admin(self):
        """Test case for poll_card_admin

        
        """
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/boss_route/polls/{pollId}'.format(poll_id=56),
            method='GET',
            headers=headers)
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_poll_card_admin_create(self):
        """Test case for poll_card_admin_create

        
        """
        body = RequestPollCardAdminCreateOrUpdate()
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/boss_route/polls/',
            method='POST',
            data=json.dumps(body),
            headers=headers,
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_poll_card_admin_update(self):
        """Test case for poll_card_admin_update

        
        """
        body = RequestPollCardAdminCreateOrUpdate()
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/boss_route/polls/{pollId}'.format(poll_id=56),
            method='POST',
            data=json.dumps(body),
            headers=headers,
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))

    def test_set_info_admin(self):
        """Test case for set_info_admin

        
        """
        body = RequestInfoSetAdmin()
        headers = [('authorization', 'authorization_example')]
        response = self.client.open(
            '/boss_routes/info/',
            method='POST',
            data=json.dumps(body),
            headers=headers,
            content_type='application/json')
        self.assert200(response,
                       'Response body is : ' + response.data.decode('utf-8'))


if __name__ == '__main__':
    import unittest
    unittest.main()
