# coding: utf-8

from __future__ import absolute_import
from datetime import date, datetime  # noqa: F401

from typing import List, Dict  # noqa: F401

from swagger_server.models.base_model_ import Model
from swagger_server import util


class ResponsePollVariant(Model):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    """
    def __init__(self, id: int=None, title: str=None, description: str=None, image_link: str=None, voted_by_user: int=None):  # noqa: E501
        """ResponsePollVariant - a model defined in Swagger

        :param id: The id of this ResponsePollVariant.  # noqa: E501
        :type id: int
        :param title: The title of this ResponsePollVariant.  # noqa: E501
        :type title: str
        :param description: The description of this ResponsePollVariant.  # noqa: E501
        :type description: str
        :param image_link: The image_link of this ResponsePollVariant.  # noqa: E501
        :type image_link: str
        :param voted_by_user: The voted_by_user of this ResponsePollVariant.  # noqa: E501
        :type voted_by_user: int
        """
        self.swagger_types = {
            'id': int,
            'title': str,
            'description': str,
            'image_link': str,
            'voted_by_user': int
        }

        self.attribute_map = {
            'id': 'id',
            'title': 'title',
            'description': 'description',
            'image_link': 'image_link',
            'voted_by_user': 'voted_by_user'
        }
        self._id = id
        self._title = title
        self._description = description
        self._image_link = image_link
        self._voted_by_user = voted_by_user

    @classmethod
    def from_dict(cls, dikt) -> 'ResponsePollVariant':
        """Returns the dict as a model

        :param dikt: A dict.
        :type: dict
        :return: The ResponsePollVariant of this ResponsePollVariant.  # noqa: E501
        :rtype: ResponsePollVariant
        """
        return util.deserialize_model(dikt, cls)

    @property
    def id(self) -> int:
        """Gets the id of this ResponsePollVariant.


        :return: The id of this ResponsePollVariant.
        :rtype: int
        """
        return self._id

    @id.setter
    def id(self, id: int):
        """Sets the id of this ResponsePollVariant.


        :param id: The id of this ResponsePollVariant.
        :type id: int
        """
        if id is None:
            raise ValueError("Invalid value for `id`, must not be `None`")  # noqa: E501

        self._id = id

    @property
    def title(self) -> str:
        """Gets the title of this ResponsePollVariant.


        :return: The title of this ResponsePollVariant.
        :rtype: str
        """
        return self._title

    @title.setter
    def title(self, title: str):
        """Sets the title of this ResponsePollVariant.


        :param title: The title of this ResponsePollVariant.
        :type title: str
        """
        if title is None:
            raise ValueError("Invalid value for `title`, must not be `None`")  # noqa: E501

        self._title = title

    @property
    def description(self) -> str:
        """Gets the description of this ResponsePollVariant.


        :return: The description of this ResponsePollVariant.
        :rtype: str
        """
        return self._description

    @description.setter
    def description(self, description: str):
        """Sets the description of this ResponsePollVariant.


        :param description: The description of this ResponsePollVariant.
        :type description: str
        """

        self._description = description

    @property
    def image_link(self) -> str:
        """Gets the image_link of this ResponsePollVariant.


        :return: The image_link of this ResponsePollVariant.
        :rtype: str
        """
        return self._image_link

    @image_link.setter
    def image_link(self, image_link: str):
        """Sets the image_link of this ResponsePollVariant.


        :param image_link: The image_link of this ResponsePollVariant.
        :type image_link: str
        """

        self._image_link = image_link

    @property
    def voted_by_user(self) -> int:
        """Gets the voted_by_user of this ResponsePollVariant.

        How many times users voted  # noqa: E501

        :return: The voted_by_user of this ResponsePollVariant.
        :rtype: int
        """
        return self._voted_by_user

    @voted_by_user.setter
    def voted_by_user(self, voted_by_user: int):
        """Sets the voted_by_user of this ResponsePollVariant.

        How many times users voted  # noqa: E501

        :param voted_by_user: The voted_by_user of this ResponsePollVariant.
        :type voted_by_user: int
        """
        if voted_by_user is None:
            raise ValueError("Invalid value for `voted_by_user`, must not be `None`")  # noqa: E501

        self._voted_by_user = voted_by_user
