import connexion
import six

from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.request_info_set_admin import RequestInfoSetAdmin  # noqa: E501
from swagger_server.models.request_poll_card_admin_create_or_update import RequestPollCardAdminCreateOrUpdate  # noqa: E501
from swagger_server.models.request_user_create_or_update_admin import RequestUserCreateOrUpdateAdmin  # noqa: E501
from swagger_server.models.request_user_search_admin import RequestUserSearchAdmin  # noqa: E501
from swagger_server.models.response_info import ResponseInfo  # noqa: E501
from swagger_server.models.response_poll_card_admin import ResponsePollCardAdmin  # noqa: E501
from swagger_server.models.response_user_admin import ResponseUserAdmin  # noqa: E501
from swagger_server.models.response_users_list_admin import ResponseUsersListAdmin  # noqa: E501
from swagger_server import util


def admin_users_card(authorization, user_id, limit=None, offset=None):  # noqa: E501
    """admin_users_card

    Get user # noqa: E501

    :param authorization: 
    :type authorization: str
    :param user_id: Requested user id
    :type user_id: int
    :param limit: 
    :type limit: int
    :param offset: 
    :type offset: int

    :rtype: ResponseUserAdmin
    """
    return 'do some magic!'


def admin_users_create(body, authorization, limit=None, offset=None):  # noqa: E501
    """admin_users_create

    Create user # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param authorization: 
    :type authorization: str
    :param limit: 
    :type limit: int
    :param offset: 
    :type offset: int

    :rtype: ResponseUserAdmin
    """
    if connexion.request.is_json:
        body = RequestUserCreateOrUpdateAdmin.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def admin_users_list(authorization, limit=None, offset=None):  # noqa: E501
    """admin_users_list

    Get list of users # noqa: E501

    :param authorization: 
    :type authorization: str
    :param limit: 
    :type limit: int
    :param offset: 
    :type offset: int

    :rtype: ResponseUsersListAdmin
    """
    return 'do some magic!'


def admin_users_search(body, authorization, limit=None, offset=None):  # noqa: E501
    """admin_users_search

    Search users # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param authorization: 
    :type authorization: str
    :param limit: 
    :type limit: int
    :param offset: 
    :type offset: int

    :rtype: ResponseUsersListAdmin
    """
    if connexion.request.is_json:
        body = RequestUserSearchAdmin.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def admin_users_update(body, authorization, user_id, limit=None, offset=None):  # noqa: E501
    """admin_users_update

    Update user # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param authorization: 
    :type authorization: str
    :param user_id: Requested user id
    :type user_id: int
    :param limit: 
    :type limit: int
    :param offset: 
    :type offset: int

    :rtype: ResponseUserAdmin
    """
    if connexion.request.is_json:
        body = RequestUserCreateOrUpdateAdmin.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def poll_card_admin(authorization, poll_id):  # noqa: E501
    """poll_card_admin

    Get poll card for admin # noqa: E501

    :param authorization: 
    :type authorization: str
    :param poll_id: Requested poll id
    :type poll_id: int

    :rtype: ResponsePollCardAdmin
    """
    return 'do some magic!'


def poll_card_admin_create(body, authorization):  # noqa: E501
    """poll_card_admin_create

    Create poll card for admin # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param authorization: 
    :type authorization: str

    :rtype: ResponsePollCardAdmin
    """
    if connexion.request.is_json:
        body = RequestPollCardAdminCreateOrUpdate.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def poll_card_admin_update(body, authorization, poll_id):  # noqa: E501
    """poll_card_admin_update

    Update poll card for admin # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param authorization: 
    :type authorization: str
    :param poll_id: Requested poll id
    :type poll_id: int

    :rtype: ResponsePollCardAdmin
    """
    if connexion.request.is_json:
        body = RequestPollCardAdminCreateOrUpdate.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def set_info_admin(body, authorization):  # noqa: E501
    """set_info_admin

    Set info # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param authorization: 
    :type authorization: str

    :rtype: ResponseInfo
    """
    if connexion.request.is_json:
        body = RequestInfoSetAdmin.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
