import connexion
import six

from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.response_am_i_bad import ResponseAmIBad  # noqa: E501
from swagger_server import util


def am_i_bad(authorization):  # noqa: E501
    """am_i_bad

    I am bad (or boss) superfeature # noqa: E501

    :param authorization: 
    :type authorization: str

    :rtype: ResponseAmIBad
    """
    return 'do some magic!'
