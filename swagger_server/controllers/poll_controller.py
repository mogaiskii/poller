import connexion
import six

from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.request_poll_card_admin_create_or_update import RequestPollCardAdminCreateOrUpdate  # noqa: E501
from swagger_server.models.request_poll_vote import RequestPollVote  # noqa: E501
from swagger_server.models.response_poll_card import ResponsePollCard  # noqa: E501
from swagger_server.models.response_poll_card_admin import ResponsePollCardAdmin  # noqa: E501
from swagger_server.models.response_polls_list import ResponsePollsList  # noqa: E501
from swagger_server import util


def poll_card(authorization, poll_id):  # noqa: E501
    """poll_card

    Get poll card # noqa: E501

    :param authorization: 
    :type authorization: str
    :param poll_id: Requested poll id
    :type poll_id: int

    :rtype: ResponsePollCard
    """
    return 'do some magic!'


def poll_card_admin(authorization, poll_id):  # noqa: E501
    """poll_card_admin

    Get poll card for admin # noqa: E501

    :param authorization: 
    :type authorization: str
    :param poll_id: Requested poll id
    :type poll_id: int

    :rtype: ResponsePollCardAdmin
    """
    return 'do some magic!'


def poll_card_admin_create(body, authorization):  # noqa: E501
    """poll_card_admin_create

    Create poll card for admin # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param authorization: 
    :type authorization: str

    :rtype: ResponsePollCardAdmin
    """
    if connexion.request.is_json:
        body = RequestPollCardAdminCreateOrUpdate.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def poll_card_admin_update(body, authorization, poll_id):  # noqa: E501
    """poll_card_admin_update

    Update poll card for admin # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param authorization: 
    :type authorization: str
    :param poll_id: Requested poll id
    :type poll_id: int

    :rtype: ResponsePollCardAdmin
    """
    if connexion.request.is_json:
        body = RequestPollCardAdminCreateOrUpdate.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def poll_vote(body, authorization, poll_id):  # noqa: E501
    """poll_vote

    Vote for poll # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param authorization: 
    :type authorization: str
    :param poll_id: Requested poll id
    :type poll_id: int

    :rtype: ResponsePollCard
    """
    if connexion.request.is_json:
        body = RequestPollVote.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'


def polls_list(authorization, limit=None, offset=None):  # noqa: E501
    """polls_list

    Get list of polls # noqa: E501

    :param authorization: 
    :type authorization: str
    :param limit: 
    :type limit: int
    :param offset: 
    :type offset: int

    :rtype: ResponsePollsList
    """
    return 'do some magic!'
