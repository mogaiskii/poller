import connexion
import six

from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.request_auth import RequestAuth  # noqa: E501
from swagger_server.models.response_auth import ResponseAuth  # noqa: E501
from swagger_server.models.success import Success  # noqa: E501
from swagger_server import util


def auth_check(authorization):  # noqa: E501
    """auth_check

    Check token # noqa: E501

    :param authorization: 
    :type authorization: str

    :rtype: Success
    """
    return 'do some magic!'


def auth_request(body):  # noqa: E501
    """auth_request

    Perform auth # noqa: E501

    :param body: 
    :type body: dict | bytes

    :rtype: ResponseAuth
    """
    if connexion.request.is_json:
        body = RequestAuth.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
