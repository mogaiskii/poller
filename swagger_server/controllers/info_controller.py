import connexion
import six

from swagger_server.models.error import Error  # noqa: E501
from swagger_server.models.request_info_set_admin import RequestInfoSetAdmin  # noqa: E501
from swagger_server.models.response_info import ResponseInfo  # noqa: E501
from swagger_server import util


def get_info(authorization):  # noqa: E501
    """get_info

    Get info # noqa: E501

    :param authorization: 
    :type authorization: str

    :rtype: ResponseInfo
    """
    return 'do some magic!'


def set_info_admin(body, authorization):  # noqa: E501
    """set_info_admin

    Set info # noqa: E501

    :param body: 
    :type body: dict | bytes
    :param authorization: 
    :type authorization: str

    :rtype: ResponseInfo
    """
    if connexion.request.is_json:
        body = RequestInfoSetAdmin.from_dict(connexion.request.get_json())  # noqa: E501
    return 'do some magic!'
