For migration, use:
`source migrate.sh "migration name"`

For run, use:
`python run.py`

Allowed envs:
PORT
JWT_TOKEN
JWT_LIFETIME
JWT_ALGORITHM
DB_NAME
DB_HOST
DB_PORT
DB_PASSWORD
DB_USER
SQLALCHEMY_ECHO


For debug:

export FLASK_APP=run.py
export FLASK_ENV=development
flask run
