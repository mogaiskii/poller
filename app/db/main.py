from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from app.app import Application
from app.configuration import Config


db_url = f"postgresql://{Application.config['DB_USER']}:{Application.config['DB_PASSWORD']}@{Application.config['DB_HOST']}:{Application.config['DB_PORT']}/{Application.config['DB_NAME']}"

Application.config['SQLALCHEMY_DATABASE_URI'] = db_url



DataBase = SQLAlchemy(Application, session_options={"autoflush": Application.config['SQLALCHEMY_ECHO']})
MigrateManager = Migrate(Application, DataBase)


