"""seed

Revision ID: 21b1a1b2caef
Revises: 18ba48daacb7
Create Date: 2020-06-20 12:47:41.020333

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.sql import table, column
from sqlalchemy import String, Integer, Text, Boolean


# revision identifiers, used by Alembic.
revision = '21b1a1b2caef'
down_revision = '18ba48daacb7'
branch_labels = None
depends_on = None


def upgrade():
    op.execute('insert into organizations(id, title, info) values (1, \'title\', \'info\')')
    op.execute('insert into users (id, org_id, code, name, is_staff) values (1, 1, \'Boss_Pass\', \'Boss\', true)')


def downgrade():
    op.execute('delete from users where id = 1')
    op.execute('delete from organizations where id = 1')

