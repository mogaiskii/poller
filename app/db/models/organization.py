from app.db import DataBase

class DBOrganization(DataBase.Model):
    __tablename__ = 'organizations'

    id = DataBase.Column(DataBase.Integer, primary_key=True)
    title = DataBase.Column(DataBase.String(255), nullable=False)
    info = DataBase.Column(DataBase.Text, nullable=False)

