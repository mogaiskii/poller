from app.db import DataBase


class DBPollRowRange(DataBase.Model):
    __tablename__ = 'poll_row_ranges'

    id = DataBase.Column(DataBase.Integer, primary_key=True)
    poll_row_id = DataBase.Column(DataBase.ForeignKey('poll_rows.id', ondelete='CASCADE'), nullable=False)
    min_value = DataBase.Column(DataBase.Integer, nullable=False, default=0, server_default='0')
    max_value = DataBase.Column(DataBase.Integer, nullable=True)

    @property
    def voted(self) -> list:
        return []

    @property
    def value(self) -> int:
        return 0


class DBPollRowRangeAdmin(DBPollRowRange):
    votes = DataBase.relationship('DBPollRowRangeUserVote', lazy='joined')
