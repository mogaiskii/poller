from app.db import DataBase


class DBPollRowRangeUserVote(DataBase.Model):
    __tablename__ = 'poll_row_range_user_vote'

    id = DataBase.Column(DataBase.Integer, primary_key=True)
    poll_id = DataBase.Column(DataBase.ForeignKey('polls.id', ondelete='CASCADE'), nullable=False)
    poll_row_id = DataBase.Column(DataBase.ForeignKey('poll_rows.id', ondelete='CASCADE'), nullable=False)
    poll_row_range_id = DataBase.Column(DataBase.ForeignKey('poll_row_ranges.id', ondelete='CASCADE'), nullable=False)
    user_id = DataBase.Column(DataBase.ForeignKey('users.id', ondelete='CASCADE'), nullable=False)
    value = DataBase.Column(DataBase.Integer, nullable=False)
