import enum
from datetime import datetime
from app.db import DataBase

class EnumPollRowTypes(enum.Enum):
    variant = 'variant'
    range = 'range'


class DBPollRow(DataBase.Model):
    __tablename__ = 'poll_rows'

    id = DataBase.Column(DataBase.Integer, primary_key=True)
    row_name = DataBase.Column(DataBase.String(255), nullable=False)
    poll_id = DataBase.Column(DataBase.ForeignKey('polls.id', ondelete='CASCADE'), nullable=False)
    row_available_votes_actions = DataBase.Column(DataBase.Integer, nullable=True)
    type = DataBase.Column(DataBase.Enum(EnumPollRowTypes), nullable=False)


class DBPollRowCard(DBPollRow):
    range = DataBase.relationship('DBPollRowRange', lazy='joined', uselist=False)
    variants = DataBase.relationship('DBPollRowVariant', lazy='joined')


class DBPollRowAdmin(DBPollRow):
    range = DataBase.relationship('DBPollRowRangeAdmin', lazy='joined', uselist=False)
    variants = DataBase.relationship('DBPollRowVariantAdmin', lazy='joined')
