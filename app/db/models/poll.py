from datetime import datetime
from app.db import DataBase

class DBPoll(DataBase.Model):
    __tablename__ = 'polls'

    id = DataBase.Column(DataBase.Integer, primary_key=True)
    poll_name = DataBase.Column(DataBase.String(255), nullable=False)
    created_at = DataBase.Column(DataBase.DateTime, nullable=False, default=datetime.utcnow)
    due_to = DataBase.Column(DataBase.DateTime, nullable=True)
    voted_users = DataBase.Column(DataBase.Integer, nullable=False, default=0, server_default='0')
    active = DataBase.Column(DataBase.Boolean, nullable=False, default=False, server_default='False')
    available_votes_actions = DataBase.Column(DataBase.Integer, nullable=True)

    @property
    def need_attention(self) -> bool:
        return False


class DBPollCard(DBPoll):
    poll_structure = DataBase.relationship('DBPollRowCard', lazy='joined')


class DBPollAdmin(DBPoll):
    poll_structure = DataBase.relationship('DBPollRowAdmin', lazy='joined')
    allowed_users = DataBase.relationship(
        'DBUser',
        uselist=True,
        secondary='user_allowed_votes',
        primaryjoin='DBPoll.id==DBUserAllowedVote.poll_id',
        secondaryjoin='DBUserAllowedVote.user_id==DBUser.id',
        lazy='joined'
    )
