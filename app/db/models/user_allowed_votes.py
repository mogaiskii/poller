# TODO: rename to user_allowed_polls

from app.db import DataBase


class DBUserAllowedVote(DataBase.Model):
    __tablename__ = 'user_allowed_votes'

    id = DataBase.Column(DataBase.Integer, primary_key=True)
    poll_id = DataBase.Column(DataBase.ForeignKey('polls.id', ondelete='CASCADE'), nullable=False)
    user_id = DataBase.Column(DataBase.ForeignKey('users.id', ondelete='CASCADE'), nullable=False)

