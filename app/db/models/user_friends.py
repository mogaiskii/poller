from app.db import DataBase


class DBUserFriend(DataBase.Model):
    __tablename__ = 'user_friends'

    id = DataBase.Column(DataBase.Integer, primary_key=True)
    left_user_id = DataBase.Column(DataBase.ForeignKey('users.id', ondelete='CASCADE'), nullable=False)
    right_user_id = DataBase.Column(DataBase.ForeignKey('users.id', ondelete='CASCADE'), nullable=False)

