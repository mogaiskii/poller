from sqlalchemy import select, func
from sqlalchemy.orm import column_property

from app.db import DataBase
from app.db.models import DBPollRowVariantUserVote


class DBPollRowVariant(DataBase.Model):
    __tablename__ = 'poll_row_variants'

    id = DataBase.Column(DataBase.Integer, primary_key=True)
    poll_row_id = DataBase.Column(DataBase.ForeignKey('poll_rows.id', ondelete='CASCADE'), nullable=False)
    title = DataBase.Column(DataBase.String(255), nullable=False)
    description = DataBase.Column(DataBase.String(255), nullable=True)
    image_link = DataBase.Column(DataBase.String(255), nullable=True)

    num_votes = column_property(
        select([func.count(DBPollRowVariantUserVote.id)]).\
            where(DBPollRowVariantUserVote.poll_row_variant_id == id).\
            correlate_except(DBPollRowVariantUserVote)
    )

    @property
    def voted(self) -> list:
        return []

    @property
    def voted_by_user(self) -> int:
        return 0



class DBPollRowVariantAdmin(DBPollRowVariant):
    voted_users = column_property(
        select([func.count(DBPollRowVariantUserVote.user_id)]).\
            where(DBPollRowVariantUserVote.poll_row_variant_id == DBPollRowVariant.id).\
            correlate_except(DBPollRowVariantUserVote)
    )
    voted_list = DataBase.relationship(
        'DBUser',
        uselist=True,
        secondary='poll_row_variant_user_vote',
        primaryjoin='DBPollRowVariant.id==DBPollRowVariantUserVote.poll_row_variant_id',
        secondaryjoin='DBPollRowVariantUserVote.user_id==DBUser.id',
        lazy='joined'
    )
