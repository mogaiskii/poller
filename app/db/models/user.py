from sqlalchemy.ext.hybrid import hybrid_property
from app.db import DataBase
from app.db.models import DBUserFriend, DBUserAllowedVote


class DBUser(DataBase.Model):
    __tablename__ = 'users'

    id = DataBase.Column(DataBase.Integer, primary_key=True)
    org_id = DataBase.Column(DataBase.ForeignKey('organizations.id', ondelete='CASCADE'), nullable=False)
    code = DataBase.Column(DataBase.String(10), nullable=False)
    name = DataBase.Column(DataBase.String(128), nullable=False)
    vk_id = DataBase.Column(DataBase.String(32), nullable=True)
    is_staff = DataBase.Column(DataBase.Boolean, nullable=False, default=False, server_default='False')
    bad_guy = DataBase.Column(DataBase.Boolean, nullable=False, default=False, server_default='False')
    phone = DataBase.Column(DataBase.String(32), nullable=True)
    blocked = DataBase.Column(DataBase.Boolean, nullable=False, default=False, server_default='False')

    @hybrid_property
    def friend_of(self) -> list:
        left = DataBase.session.query(DBUserFriend.left_user_id).filter(DBUserFriend.right_user_id == self.id)
        right = DataBase.session.query(DBUserFriend.right_user_id).filter(DBUserFriend.left_user_id == self.id)
        users = left.union(right).all()
        return [u[0] for u in users]

    @hybrid_property
    def allowed_votes(self) -> list:
        votes = DataBase.session.query(DBUserAllowedVote.poll_id).filter(DBUserAllowedVote.user_id == self.id).all()
        return [v[0] for v in votes]
