__all__ = ['Application']

from datetime import date
import traceback
from flask import Flask, json, Response, jsonify
from flask.json import JSONEncoder
from werkzeug.exceptions import HTTPException
from dotenv import load_dotenv
from flask_cors import CORS

from .core.common.exceptions import ApplicationException
from swagger_server.models import Error


load_dotenv(verbose=True)

Application = Flask(__name__)

Application.config.from_object('app.configuration.Config')


@Application.errorhandler(Exception)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    traceback.print_exc()
    if isinstance(e, ApplicationException):
        response_model = Error(code=e.code, message=e.message)
        response_content_type = "application/json"
        return Response(response=response_model.to_dict(), status=e.status, mimetype=response_content_type)

    elif isinstance(e, HTTPException):
        response = e.get_response()
        # replace the body with JSON
        response_model = Error(code=e.code, message=e.description)
        response.data = json.dumps(response_model.to_dict())
        response.content_type = "application/json"
        return response

    else:
        response_data = json.dumps({
            "code": 0,
            "message": "Internal Error",
        })
        response_content_type = "application/json"
        response = Response(response=response_data, status=500, mimetype=response_content_type)
        return response


class CustomJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, date):
                return obj.isoformat()
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


Application.json_encoder = CustomJSONEncoder

CORS(Application)

# from app.api.routes import *
from app.db import *

