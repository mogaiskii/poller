import os


def BoolField(value, env):
    value = os.environ.get(env, value)
    if str(value)[0].lower() in ['t', '1']:
        return True
    return False


# TODO: fix this hack
def Field(value, env, type_wrapper=str):
    return type_wrapper( os.environ.get(env, value) )


class Config:
    PORT        = Field(5000, 'PORT')
    DB_NAME     = Field('poller', 'DB_NAME')
    DB_HOST     = Field('0.0.0.0', 'DB_HOST')
    DB_PORT     = Field('5432', 'DB_PORT')
    DB_PASSWORD = Field('mPmksh9r6gxTu5GemPCK', 'DB_PASSWORD')
    DB_USER     = Field('postgres', 'DB_USER')

    JWT_TOKEN   = Field('ASDna820z', 'JWT_TOKEN')
    JWT_ALGORITHM   = Field('HS256', 'JWT_ALGORITHM')
    JWT_LIFETIME   = Field(60*60*24*30, 'JWT_LIFETIME', int)

    SQLALCHEMY_ECHO = BoolField(True, 'SQLALCHEMY_ECHO')
