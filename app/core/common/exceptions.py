class ApplicationException(Exception):
    code = 0
    status = 500
    message = ''

    def __init__(self, message = None):
        self.message = message or ''


class ValidationException(ApplicationException):
    code = 1
    status = 400
