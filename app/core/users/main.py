import structlog

from app.db.models import DBUser, DBUserFriend, DBUserAllowedVote, DBPoll
from app.db import DataBase
from app.core.common.exceptions import ValidationException, ApplicationException

logger = structlog.get_logger('core.users')


def validate_user_form(code, vk_id, is_staff, phone, friend_of=None, allowed_votes=None, id=None, *args, **kwargs):
    """
        rises ApplicationException, ValidationException
    """

    # match code not exists
    code_owner = DataBase.session.query(DBUser.id).filter(DBUser.code == code).scalar()

    if code_owner is not None:
        # if len(code_owner) > 1:
        #     logger.error(f'more than one code owner. code: {code}')
        #     raise ApplicationException('more than one code owner')

        if id is not None:
            if str(code_owner) != str(id):
                raise ValidationException('code exists')
        else:
            raise ValidationException('code exists')

    if friend_of is not None:
        friends_check = DataBase.session.query(DBUser.id).filter(DBUser.id.in_(friend_of)).all()
        friends_check = [u[0] for u in friends_check]

        if friends_check is None or len(friends_check) < len(friend_of):
            raise ValidationException('wrong friend_of list')

    if allowed_votes is not None:
        votes_check = DataBase.session.query(DBPoll.id).filter(DBPoll.id.in_(allowed_votes)).all()
        votes_check = [v[0] for v in votes_check]

        if votes_check is None or len(votes_check) < len(allowed_votes):
            raise ValidationException('wrong allowed_votes list')




def create_user(name, code, vk_id, is_staff, phone, friend_of, allowed_votes, bad_guy=False, blocked=False, *args, **kwargs) -> DBUser:
    user = DBUser(
            # org_id = request_model.org_id
            org_id=1, # NOTE: HARDCODE
            name=name,
            code=code,
            vk_id=vk_id,
            is_staff=is_staff,
            phone=phone,
            bad_guy=bad_guy,
            blocked=blocked
        )

    DataBase.session.add(user)
    DataBase.session.flush([user])

    update_user_relations(user, friend_of, allowed_votes)

    return user


def update_user(user: DBUser, name=None, code=None, vk_id=None, is_staff=None, phone=None, friend_of=None, allowed_votes=None, bad_guy=None, blocked=None, *args, **kwargs) -> DBUser:

    if code is not None:
        user.code = code

    if name is not None:
        user.name = name

    if vk_id is not None:
        user.vk_id = vk_id

    if is_staff is not None:
        user.is_staff = is_staff

    if phone is not None:
        user.phone = phone

    if bad_guy is not None:
        user.bad_guy = bad_guy

    if blocked is not None:
        user.blocked = blocked

    DataBase.session.add(user)

    update_user_relations(user, friend_of, allowed_votes)

    return user


def update_user_relations(user, friend_of: list=None, allowed_votes: list=None):
    models_to_flush = []

    if friend_of is not None:
        DataBase.session.query(DBUserFriend).filter(
            (DBUserFriend.left_user_id == user.id) | (DBUserFriend.right_user_id == user.id)
            ).delete(synchronize_session='fetch')

        for uid in friend_of:
                friend = DBUserFriend(
                    left_user_id=user.id,
                    right_user_id=uid
                )
                DataBase.session.add(friend)
                models_to_flush.append(friend)

        # TODO:
        # setattr(user, 'friend_of', friend_of)

    if allowed_votes is not None:
        DataBase.session.query(DBUserAllowedVote).filter(DBUserAllowedVote.user_id == user.id).delete(synchronize_session='fetch')

        for vote_id in allowed_votes:
            vote = DBUserAllowedVote(
                poll_id=vote_id,
                user_id=user.id
            )
            DataBase.session.add(vote)
            models_to_flush.append(vote)

        # TODO:
        # setattr(user, 'allowed_votes', allowed_votes)

    DataBase.session.flush(models_to_flush)

