import datetime
from typing import Optional

import jwt
from sqlalchemy.orm.exc import NoResultFound

from app import Application
from app.db import DataBase
from app.db.models import DBUser
from app.configuration import Config
from .jwt_model import JWTModelSchema


def get_auth_token(user: DBUser) -> str:
    expired_at = datetime.datetime.utcnow() + datetime.timedelta(seconds=Application.config['JWT_LIFETIME'])
    data = JWTModelSchema().dump({"id": user.id, "expire_at": expired_at})
    token = jwt.encode(data, Application.config['JWT_TOKEN'], algorithm=Application.config['JWT_ALGORITHM'])
    return token.decode("utf-8")


def authorize(code: str) -> Optional[DBUser]:
    try:
        user = DataBase.session.query(DBUser).filter(DBUser.code == code).one()
    except NoResultFound:
        return None

    return user
