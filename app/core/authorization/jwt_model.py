from marshmallow import Schema, fields


class JWTModelSchema(Schema):
    id = fields.Integer(required=True, allow_none=False)
    expire_at = fields.DateTime(required=True, allow_none=False)
