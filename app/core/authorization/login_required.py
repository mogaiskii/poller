from datetime import datetime
from functools import wraps

import structlog
import jwt
from flask import g, request, redirect, url_for
from jwt.exceptions import InvalidTokenError
from werkzeug.exceptions import Unauthorized, Forbidden
from marshmallow import ValidationError
from sqlalchemy.orm.exc import NoResultFound

from app import Application
from app.configuration import Config
from app.db import DataBase
from app.db.models import DBUser
from .jwt_model import JWTModelSchema


logger = structlog.get_logger('login_required')

REQUEST_USER_KWARG = 'request_user'


def login_required(staff_only: bool = False):
    def login_required_wrapper(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            header = request.headers.get('Authorization')

            try:
                user_info = jwt.decode(header, Application.config['JWT_TOKEN'], algorithms=[Application.config['JWT_ALGORITHM']])
                user_info_model = JWTModelSchema().load(user_info)

                if user_info_model['expire_at'] <= datetime.utcnow():
                    raise Unauthorized

                user: DBUser = DataBase.session.query(DBUser).filter(DBUser.id == user_info_model['id']).one()

                if user.blocked is True:
                    raise Forbidden('Banned')

                if staff_only is True:
                    if user.is_staff is not True:
                        raise Forbidden

                kwargs[REQUEST_USER_KWARG] = user

            except (InvalidTokenError, ValidationError, NoResultFound):
                raise Unauthorized
            except Exception as e:
                logger.error('Error in login_requried')
                try:
                    logger.error(e)
                except Exception:
                    pass

                raise e

            return f(*args, **kwargs)
        return decorated_function

    if callable(staff_only):
        return login_required_wrapper(staff_only)

    return login_required_wrapper