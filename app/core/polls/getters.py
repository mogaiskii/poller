from sqlalchemy import select
from sqlalchemy.sql import exists, func
from sqlalchemy.orm import column_property

from app.db import DataBase
from app.db.models import DBUser, DBPollCard, DBPollRowCard, DBPollRowRange, \
    DBPollRowRangeUserVote, DBPollRowVariant, DBPollRowVariantUserVote


def get_personized_user_poll_card(user: DBUser) -> DBPollCard:

    DBPollRowVariantPersonized = type('DBPollRowVariantPersonized', (DBPollRowVariant, ), dict())
    DBPollRowVariantPersonized.voted_by_user = column_property(
        select([func.count(DBPollRowVariantUserVote.value)])\
            .where(DBPollRowVariantUserVote.poll_row_variant_id == DBPollRowVariantPersonized.id)\
            .where(DBPollRowVariantUserVote.user_id == user.id)\
            .where(DBPollRowVariantUserVote.value == True)
    )

    DBPollRowRangePersonized = type('DBPollRowRangePersonized', (DBPollRowRange, ), dict())
    DBPollRowRangePersonized.voted = column_property(exists().where(DBPollRowRangeUserVote.user_id == user.id))
    DBPollRowRangePersonized.value = column_property(
        select([DBPollRowRangeUserVote.value])\
            .where(DBPollRowRangeUserVote.user_id == user.id)\
            .where(DBPollRowRangeUserVote.poll_row_range_id == DBPollRowRangePersonized.id)
    )

    DBPollRowPersonalized = type('DBPollRowPersonalized', (DBPollRowCard, ), dict())
    DBPollRowPersonalized.range = DataBase.relationship(DBPollRowRangePersonized, lazy='joined', uselist=False)
    DBPollRowPersonalized.variants = DataBase.relationship(DBPollRowVariantPersonized, lazy='joined', uselist=True)

    DBPollPersonalized = type('DBPollPersonalized', (DBPollCard, ), dict())
    DBPollPersonalized.poll_structure = DataBase.relationship(DBPollRowPersonalized, lazy='joined', uselist=True)
    # TODO: poll.need_attention = column_property

    return DBPollPersonalized



