def validate_poll_admin_form(*args, **kwargs):
    pass


def validate_poll_user_vote(poll, poll_structure, *args, **kwargs):
    used_votes = 0
    for row in poll_structure:

        row_model = None
        for item in poll.poll_structure:
            if item.id == row.id:
                row_model = item
                break
        if row_model is None:
            raise ValidationException('wrong poll structure')

        row_votes = 0

        if getattr(row, 'variants', None) is not None:
            for var in row.variants:

                variant_model = None
                for item in row_model.variants:
                    if item.id == var.id:
                        variant_model = item
                        break
                if variant_model is None:
                    raise ValidationException('wrong poll structure')

                if var.voted_by_user > 0:
                    row_votes += var.voted_by_user
                    used_votes += var.voted_by_user
                    if used_votes > poll.available_votes_actions:
                        raise ValidationException('vote limit exceeded')
                    if used_votes > row_model.row_available_votes_actions:
                        raise ValidationException('vote limit exceeded')

        if getattr(row, 'range', None) is not None:
            range_model = getattr(row_model, 'range', None)
            if range_model is None:
                raise ValidationException('wrong poll structure')
            if not (range_model.min_value <= row.range.value <= range_model.max_value):
                raise ValidationException('wrong range')
