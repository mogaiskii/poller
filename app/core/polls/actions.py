import datetime
from typing import List

from sqlalchemy.orm.exc import NoResultFound

from app.db import DataBase
from app.db.models import DBPollAdmin, DBPollRowAdmin, DBPollRowRangeAdmin, DBPollRowVariantAdmin, DBUserAllowedVote, \
    DBPollRowVariantUserVote, DBPollRowRangeUserVote



def create_poll(poll_name:str, active:bool, available_votes_actions:int, allowed_users:List[int], poll_structure:List[dict], due_to:datetime.datetime=None, *args, **kwargs) -> DBPollAdmin:
    poll = DBPollAdmin(
        poll_name=poll_name,
        created_at=datetime.datetime.utcnow(),
        due_to=due_to,
        voted_users=0,
        available_votes_actions=available_votes_actions,
        active=active
    )

    DataBase.session.add(poll)
    DataBase.session.flush([poll])

    update_poll_structure(poll, poll_structure)
    update_poll_users(poll, allowed_users)

    return poll


def update_poll(poll, poll_name:str, active:bool, available_votes_actions:int, allowed_users:List[int], poll_structure:List[dict], due_to:datetime.datetime=None, *args, **kwargs) -> DBPollAdmin:
    poll.poll_name = poll_name
    poll.due_to = due_to
    poll.active = active
    poll.available_votes_actions = available_votes_actions

    DataBase.session.add(poll)

    DataBase.session.flush([poll])

    update_poll_structure(poll, poll_structure)
    update_poll_users(poll, allowed_users)

    return poll


def update_poll_structure(poll, poll_structure:List[dict]):
    row_models = []
    for row_data in poll_structure:
        row_model = update_or_create_row(poll=poll, **row_data)
        row_models.append(row_model)

    row_models_ids = [row.id for row in row_models]
    DataBase.session.query(DBPollRowAdmin).filter(DBPollRowAdmin.poll_id==poll.id).filter(DBPollRowAdmin.id.notin_(row_models_ids)).delete(synchronize_session='fetch')

    setattr(poll, 'poll_structure', row_models)

    return row_models


def update_or_create_row(poll, id=None, row_name=None, poll_id=None, row_available_votes_actions=None, type=None, range=None, variants=None) -> DBPollRowAdmin:
    if id is not None:
        return update_row(poll, id, row_name, poll_id, row_available_votes_actions, type, range, variants)
    return create_row(poll, row_name, poll_id, row_available_votes_actions, type, range, variants)


def update_row(poll, id, row_name, poll_id, row_available_votes_actions, type, range, variants) -> DBPollRowAdmin:
    old_row = None
    for item in getattr(poll, 'poll_structure', []):
        if item.id == id:
            old_row = item

    if old_row is None:
        return create_row(row_name, poll.id, row_available_votes_actions, type, range, variants)

    old_row.row_name = row_name
    old_row.row_available_votes_actions = row_available_votes_actions
    old_row.type = type

    update_row_relations(old_row, range, variants)

    return old_row


def create_row(poll, row_name, poll_id, row_available_votes_actions, type, range, variants) -> DBPollRowAdmin:
    row = DBPollRowAdmin(
        row_name=row_name,
        poll_id=poll.id,
        row_available_votes_actions=row_available_votes_actions,
        type=type
    )
    DataBase.session.add(row)
    DataBase.session.flush([row])

    update_row_relations(row, range, variants)

    return row


def update_row_relations(row, range, variants):
    if variants is not None:
        row_variants = update_row_variants(row, variants)
        setattr(row, 'variants', row_variants)
    elif range is not None:
        row_range = update_or_create_row_range(row, **range)
        setattr(row, 'range', row_range)
    return row


def update_row_variants(row, variants_data):
    row_variants = []
    for var in variants_data:
        row_var = update_or_create_row_variant(row, **var)
        row_variants.append(row_var)

    row_variants_ids = [variant.id for variant in row_variants]
    DataBase.session.query(DBPollRowVariantAdmin).filter(DBPollRowVariantAdmin.poll_row_id==row.id).filter(DBPollRowVariantAdmin.id.notin_(row_variants_ids)).delete(synchronize_session='fetch')
    setattr(row, 'variants', row_variants)
    return row_variants


def update_or_create_row_range(row, id=None, min_value=None, max_value=None, *args, **kwargs):
    if getattr(row, 'range', None) is not None and row.range.id == id:
        row_range = row.range
        row_range.min_value = min_value or 0
        row_range.max_value = max_value
        DataBase.session.add(row_range)
        DataBase.session.flush([row_range])
        setattr(row, 'range', row_range)
        return row_range
    else:
        row_range = DBPollRowRangeAdmin(
            poll_row_id=row.id,
            min_value=min_value or 0,
            max_value=max_value
        )
        DataBase.session.add(row_range)
        DataBase.session.flush([row_range])
        setattr(row, 'range', row_range)
        return row_range


def update_or_create_row_variant(row, id=None, title=None, description=None, image_link=None) -> DBPollRowVariantAdmin:
    if getattr(row, 'variants', None) is not None:
        var_model = None
        for var in row.variants:
            if var.id == id:
                return update_row_variant_item(var, id, title, description, image_link)

    row_variant = DBPollRowVariantAdmin(
        poll_row_id=row.id,
        title=title,
        description=description,
        image_link=image_link
    )
    DataBase.session.add(row_variant)
    DataBase.session.flush([row_variant])
    return row_variant


def update_row_variant_item(variant, id=None, title=None, description=None, image_link=None):
    variant.title = title
    variant.description = description
    variant.image_link = image_link
    DataBase.session.add(variant)
    DataBase.session.flush([variant])
    return variant



def update_poll_users(poll, allowed_users:List[int]):
    DataBase.session.query(DBUserAllowedVote).filter(DBUserAllowedVote.poll_id==poll.id).delete(synchronize_session='fetch')

    models = []
    for user_id in allowed_users:
        allowed_one = DBUserAllowedVote(poll_id=poll.id, user_id=user_id)
        DataBase.session.add(allowed_one)
        models.append(allowed_one)

    if models:
        DataBase.session.flush(models)

    DataBase.session.query(DBPollRowVariantUserVote).filter(DBPollRowVariantUserVote.poll_id==poll.id).filter(DBPollRowVariantUserVote.user_id.notin_(allowed_users)).delete(synchronize_session='fetch')
    DataBase.session.query(DBPollRowRangeUserVote).filter(DBPollRowRangeUserVote.poll_id==poll.id).filter(DBPollRowRangeUserVote.user_id.notin_(allowed_users)).delete(synchronize_session='fetch')

    # setattr(poll, 'allowed_users', ???)

    return models


def vote_poll(poll, user, poll_structure):
    """
        NOTE: NEED VALIDATE FIRST
    """
    vote_models = []

    for row in poll_structure:

        row_model = None
        for item in poll.poll_structure:
            if item.id == row.id:
                row_model = item
                break

        if getattr(row, 'variants', None) is not None:
            for var in row.variants:

                variant_model = None
                for item in row_model.variants:
                    if item.id == var.id:
                        variant_model = item
                        break

                if var.voted_by_user > 0:
                    for _ in range(var.voted_by_user):
                        vote_models.append(DBPollRowVariantUserVote(
                            poll_id=poll.id,
                            poll_row_id = row_model.id,
                            poll_row_variant_id=variant_model.id,
                            user_id=user.id,
                            value=True
                        ))

        if getattr(row, 'range', None) is not None:
            range_model = getattr(row_model, 'range', None)

            vote_models.append(DBPollRowRangeUserVote(
                poll_id=poll.id,
                poll_row_id=row_model.id,
                poll_row_range_id=range_model.id,
                user_id=user.id,
                value=row.range.value
            ))

    DataBase.session.query(DBPollRowRangeUserVote).filter(DBPollRowRangeUserVote.user_id == user.id).filter(DBPollRowRangeUserVote.poll_id == poll.id).delete(synchronize_session='fetch')
    DataBase.session.query(DBPollRowVariantUserVote).filter(DBPollRowVariantUserVote.user_id == user.id).filter(DBPollRowVariantUserVote.poll_id == poll.id).delete(synchronize_session='fetch')

    DataBase.session.add_all(vote_models)
    DataBase.session.flush(vote_models)

    return vote_models
