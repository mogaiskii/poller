import structlog

from app.db.models import DBOrganization
from app.db import DataBase
from app.core.common.exceptions import ValidationException


logger = structlog.get_logger('core.organizations')


def update_info(organization: DBOrganization, title = None, info = None) -> DBOrganization:
    if title is not None:
        organization.title = title

    if info is not None:
        organization.info = info

    return organization
