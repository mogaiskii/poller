from app.db.models import DBPollCard, DBPollAdmin
from swagger_server.models import ResponsePollCard, ResponsePollRow, PollRange, ResponsePollVariant, \
    ResponsePollCardAdmin, ResponsePollRowAdmin, ResponsePollVariantAdmin, PollRangeAdmin, PollRangeVoteAdmin


def get_user_poll_response(poll: DBPollCard) -> ResponsePollCard:
    poll_structure = []

    for row in sorted(poll.poll_structure, key=lambda x: x.id):
        range_response = None
        if getattr(row, 'range', None) is not None and row.range:
            range = row.range
            range_response = PollRange(
                id=range.id,
                min_value=range.min_value,
                max_value=range.max_value,
                value=range.value,
                voted=range.voted
            )

        variants = []
        for variant in sorted(getattr(row, 'variants', []), key=lambda x: x.id):
            variant_response = ResponsePollVariant(
                id=variant.id,
                title=variant.title,
                description=variant.description,
                image_link=variant.image_link,
                voted_by_user=variant.voted_by_user
            )
            variants.append(variant_response)

        row_response = ResponsePollRow(
            id=row.id,
            row_name=row.row_name,
            row_available_votes_actions=row.row_available_votes_actions,
            type=row.type.value,
            range=range_response,
            variants=variants
        )
        poll_structure.append(row_response)

    response = ResponsePollCard(
        id=poll.id,
        poll_name=poll.poll_name,
        created_at=poll.created_at,
        due_to=poll.due_to,
        voted_users=poll.voted_users,
        active=poll.active,
        available_votes_actions=poll.available_votes_actions,
        need_attention=poll.active,
        poll_structure=poll_structure
    )

    return response


def get_admin_poll_response(poll: DBPollAdmin) -> ResponsePollCardAdmin:
    poll_structure = []

    for row in sorted(poll.poll_structure, key=lambda x: x.id):
        range_response = None
        if getattr(row, 'range', None) is not None:
            range = row.range

            votes = []

            for vote in getattr(range, 'votes', []):
                vote_response = PollRangeVoteAdmin(
                    id=vote.id,
                    uid=vote.user_id,
                    value=vote.value
                )
                votes.append(vote_response)

            range_response = PollRangeAdmin(
                id=range.id,
                min_value=range.min_value,
                max_value=range.max_value,
                value=range.value,
                voted=range.voted,
                votes=votes
            )

        variants = []
        for variant in sorted(getattr(row, 'variants', []), key=lambda x: x.id):
            variant_response = ResponsePollVariantAdmin(
                id=variant.id,
                title=variant.title,
                description=variant.description,
                image_link=variant.image_link,
                voted_by_user=variant.voted_by_user,
                num_votes=variant.num_votes,
                voted_list=[u.id for u in getattr(variant, 'voted_list', [])]
            )
            variants.append(variant_response)

        row_response = ResponsePollRowAdmin(
            id=row.id,
            row_name=row.row_name,
            row_available_votes_actions=row.row_available_votes_actions,
            type=row.type.value,
            range=range_response,
            variants=variants
        )
        poll_structure.append(row_response)

    response = ResponsePollCardAdmin(
        id=poll.id,
        poll_name=poll.poll_name,
        created_at=poll.created_at,
        due_to=poll.due_to,
        voted_users=poll.voted_users,
        active=poll.active,
        available_votes_actions=poll.available_votes_actions,
        poll_structure=poll_structure,
        allowed_users=[u.id for u in poll.allowed_users]
    )

    return response