from flask import request, jsonify
import structlog
from werkzeug.exceptions import Unauthorized, InternalServerError, BadRequest
from sqlalchemy.orm.exc import NoResultFound

from app import Application
from app.core.authorization import login_required
from app.core.common.exceptions import ApplicationException, ValidationException
from app.db.models import DBOrganization
from app.db import DataBase
from swagger_server.models import ResponseInfo


logger = structlog.get_logger('get_info_endpoint')


@Application.route('/info/', methods=['GET'])
@login_required
def get_info_endpoint(*args, **kwargs):
    try:

        try:
            # TODO: fix it!
            organization: DBOrganization = DataBase.session.query(DBOrganization).one()
            assert organization.id == 1
        except (NoResultFound, AssertionError, AttributeError) as e:
            logger.error(f'Error with organizaion!: {e}')
            raise e

        response = ResponseInfo(
            title=organization.title,
            text=organization.info
        )

        return jsonify(response.to_dict())
    except Exception as e:
        logger.error(f'get_info_endpoint endpoint erorr: {e}')
        raise
