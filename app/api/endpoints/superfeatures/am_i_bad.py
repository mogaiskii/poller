from flask import request, jsonify
import structlog
from werkzeug.exceptions import Unauthorized, InternalServerError, BadRequest
from sqlalchemy.orm.exc import NoResultFound

from app import Application
from app.core.authorization import login_required, REQUEST_USER_KWARG
from app.core.common.exceptions import ApplicationException, ValidationException
from app.db.models import DBUser, DBOrganization
from app.db import DataBase
from swagger_server.models import ResponseAmIBad


logger = structlog.get_logger('am_i_bad_endpoint')


@Application.route('/superfeatures/am_i_bad/', methods=['GET'])
@login_required
def am_i_bad_endpoint(*args, **kwargs):
    try:

        try:
            # TODO: fix it!
            organization: DBOrganization = DataBase.session.query(DBOrganization).one()
            assert organization.id == 1
        except (NoResultFound, AssertionError, AttributeError) as e:
            logger.error(f'Error with organizaion!: {e}')
            raise e

        user: DBUser = kwargs[REQUEST_USER_KWARG]

        response = ResponseAmIBad(
            i_am_bad=user.bad_guy
        )

        if user.is_staff is True:
            response.no__i_am_boss_ass_bitch = True


        response_dict = response.to_dict()
        if 'no__i_am_boss_ass_bitch' in response_dict and not user.is_staff:
            del response_dict['no__i_am_boss_ass_bitch']


        return jsonify(response_dict)
    except Exception as e:
        logger.error(f'am_i_bad_endpoint endpoint erorr: {e}')
        raise
