from flask import request, jsonify
import structlog
from werkzeug.exceptions import NotFound, InternalServerError
from sqlalchemy.orm.exc import NoResultFound

from app import Application
from app.core.authorization import login_required
from app.db.models import DBUser
from app.db import DataBase
from swagger_server.models import RequestUserCreateOrUpdateAdmin, ResponseUsersListAdmin, ResponseUserAdmin


logger = structlog.get_logger('create_user')


@Application.route('/boss_route/users/', methods=['GET'])
@login_required(staff_only=True)
def users_list_endpoint(*args, **kwargs):
    try:
        limit = request.args.get('limit', 30)
        offset = request.args.get('offset', 0)

        try:
            users = DataBase.session.query(DBUser).limit(limit).offset(offset).all()
        except NoResultFound:
            raise NotFound

        response = ResponseUsersListAdmin([
            ResponseUserAdmin(
                id=user.id,
                name=user.name,
                code=user.code,
                vk_id=user.vk_id,
                is_staff=user.is_staff,
                bad_guy=user.bad_guy,
                blocked=user.blocked,
                phone=user.phone,
                friend_of=user.friend_of,
                allowed_votes=user.allowed_votes
            ) for user in users
        ])

        return jsonify(response.to_dict())
    except Exception as e:
        logger.error(f'users_list_endpoint endpoint erorr: {e}')
        raise

