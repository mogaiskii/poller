from flask import request, jsonify
import structlog
from werkzeug.exceptions import NotFound, InternalServerError
from sqlalchemy.orm.exc import NoResultFound

from app import Application
from app.core.authorization import login_required
from app.db.models import DBUser
from app.db import DataBase
from swagger_server.models import RequestUserCreateOrUpdateAdmin, ResponseUserAdmin


logger = structlog.get_logger('create_user')


@Application.route('/boss_route/users/<uid>', methods=['GET'])
@login_required(staff_only=True)
def user_card_endpoint(uid, *args, **kwargs):
    try:
        try:
            user: DBUser = DataBase.session.query(DBUser).filter(DBUser.id == uid).one()
        except NoResultFound:
            raise NotFound

        response = ResponseUserAdmin(
            id=user.id,
            name=user.name,
            code=user.code,
            vk_id=user.vk_id,
            is_staff=user.is_staff,
            bad_guy=user.bad_guy,
            blocked=user.blocked,
            phone=user.phone,
            friend_of=user.friend_of,
            allowed_votes=user.allowed_votes
        )

        return jsonify(response.to_dict())
    except Exception as e:
        logger.error(f'user_card_endpoint endpoint erorr: {e}')
        raise

