from flask import request, jsonify
import structlog
from werkzeug.exceptions import Unauthorized, InternalServerError, BadRequest, NotFound
from sqlalchemy.orm.exc import NoResultFound

from app import Application
from app.core.authorization import login_required
from app.core.users import validate_user_form, update_user
from app.core.common.exceptions import ApplicationException, ValidationException
from app.db.models import DBUser
from app.db import DataBase
from swagger_server.models import RequestUserCreateOrUpdateAdmin, ResponseUserAdmin


logger = structlog.get_logger('create_user')


@Application.route('/boss_route/users/<uid>', methods=['POST'])
@login_required(staff_only=True)
def update_user_endpoint(uid, *args, **kwargs):
    try:
        request_model = RequestUserCreateOrUpdateAdmin().from_dict(request.json)

        try:
            # TODO: need more validation
            user = DBUser.query.filter(DBUser.id == uid).one()
        except NoResultFound:
            raise NotFound

        try:
            data = request_model.to_dict()
            data['id'] = uid
            validate_user_form(**data)
        except ApplicationException:
            raise
        except ValidationException as e:
            raise BadRequest(description=ValidationException.message)

        user = update_user(user, **request_model.to_dict())

        DataBase.session.commit()

        response = ResponseUserAdmin(
            id=user.id,
            name=user.name,
            code=user.code,
            vk_id=user.vk_id,
            is_staff=user.is_staff,
            bad_guy=user.bad_guy,
            blocked=user.blocked,
            phone=user.phone,
            friend_of = user.friend_of,
            allowed_votes=user.allowed_votes
        )

        return jsonify(response.to_dict())
    except Exception as e:
        logger.error(f'update_user_endpoint endpoint erorr: {e}')
        raise

