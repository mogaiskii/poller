from flask import request, jsonify
import structlog
from werkzeug.exceptions import NotFound, InternalServerError
from sqlalchemy.orm.exc import NoResultFound

from app import Application
from app.core.authorization import login_required
from app.db.models import DBUser
from app.db import DataBase
from swagger_server.models import RequestUserSearchAdmin, ResponseUsersListAdmin, ResponseUserAdmin


logger = structlog.get_logger('create_user')


@Application.route('/boss_route/users/search/', methods=['post'])
@login_required(staff_only=True)
def users_search_endpoint(*args, **kwargs):
    try:
        limit = request.args.get('limit', 30)
        offset = request.args.get('offset', 0)

        request_model = RequestUserSearchAdmin().from_dict(request.json)

        try:
            query = DataBase.session.query(DBUser)

            if getattr(request_model, 'ids', None) is not None:
                query = query.filter(DBUser.id.in_(request_model.ids))

            if getattr(request_model, 'name', None) is not None:
                query = query.filter(DBUser.name.ilike(f'%{request_model.name}%'))

            if getattr(request_model, 'phone', None) is not None:
                query = query.filter(DBUser.phone.ilike(f'%{request_model.phone}%'))

            if getattr(request_model, 'vk_id', None) is not None:
                query = query.filter(DBUser.vk_id.ilike(f'%{request_model.vk_id}%'))

            users = query.limit(limit).offset(offset).all()
        except NoResultFound:
            raise NotFound

        response = ResponseUsersListAdmin([
            ResponseUserAdmin(
                id=user.id,
                name=user.name,
                code=user.code,
                vk_id=user.vk_id,
                is_staff=user.is_staff,
                bad_guy=user.bad_guy,
                blocked=user.blocked,
                phone=user.phone,
                friend_of=user.friend_of,
                allowed_votes=user.allowed_votes
            ) for user in users
        ])

        return jsonify(response.to_dict())
    except Exception as e:
        logger.error(f'users_search_endpoint endpoint erorr: {e}')
        raise

