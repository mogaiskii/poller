from flask import request, jsonify
import structlog
from werkzeug.exceptions import Unauthorized, InternalServerError, BadRequest

from app import Application
from app.core.authorization import login_required
from app.core.users import validate_user_form, create_user
from app.core.common.exceptions import ApplicationException, ValidationException
from app.db.models import DBUser, DBUserFriend, DBUserAllowedVote
from app.db import DataBase
from swagger_server.models import RequestUserCreateOrUpdateAdmin, ResponseUserAdmin


logger = structlog.get_logger('create_user')


@Application.route('/boss_route/users/', methods=['POST'])
@login_required(staff_only=True)
def create_user_endpoint(*args, **kwargs):
    try:
        request_model = RequestUserCreateOrUpdateAdmin().from_dict(request.json)

        try:
            # TODO: need more validation
            validate_user_form(**request_model.to_dict())
        except ApplicationException:
            raise
        except ValidationException as e:
            raise BadRequest(description=ValidationException.message)

        user = create_user(**request_model.to_dict())

        DataBase.session.commit()

        response = ResponseUserAdmin(
            id=user.id,
            name=user.name,
            code=user.code,
            vk_id=user.vk_id,
            is_staff=user.is_staff,
            bad_guy=user.bad_guy,
            blocked=user.blocked,
            phone=user.phone,
            friend_of=request_model.friend_of,
            allowed_votes=request_model.allowed_votes
        )

        return jsonify(response.to_dict())
    except Exception as e:
        logger.error(f'create_user_endpoint endpoint erorr: {e}')
        raise

