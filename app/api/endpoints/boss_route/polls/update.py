from flask import request, jsonify
import structlog
from werkzeug.exceptions import NotFound, BadRequest
from sqlalchemy.orm.exc import NoResultFound

from app import Application
from app.api.helpers.polls import get_admin_poll_response
from app.core.authorization import login_required
from app.core.polls import validate_poll_admin_form, update_poll
from app.core.common.exceptions import ApplicationException, ValidationException
from app.db.models import DBPollAdmin
from app.db import DataBase
from swagger_server.models import RequestPollCardAdminCreateOrUpdate, ResponsePollCardAdmin


logger = structlog.get_logger('update_poll_endpoint')


@Application.route('/boss_route/polls/<poll_id>', methods=['POST'])
@login_required(staff_only=True)
def update_poll_endpoint(poll_id, *args, **kwargs):
    try:
        request_model = RequestPollCardAdminCreateOrUpdate().from_dict(request.json)

        try:
            # TODO: need more validation
            validate_poll_admin_form(**request_model.to_dict())
        except ApplicationException:
            raise
        except ValidationException as e:
            raise BadRequest(description=ValidationException.message)

        try:
            poll: DBPollAdmin = DataBase.session.query(DBPollAdmin).filter(DBPollAdmin.id == poll_id).one()
        except NoResultFound:
            raise NotFound

        poll = update_poll(poll, **request_model.to_dict())

        DataBase.session.commit()

        response = get_admin_poll_response(poll)

        return jsonify(response.to_dict())
    except Exception as e:
        logger.error(f'update_poll_endpoint endpoint erorr: {e}')
        raise

