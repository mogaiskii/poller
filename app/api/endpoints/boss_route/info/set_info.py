from flask import request, jsonify
import structlog
from werkzeug.exceptions import Unauthorized, InternalServerError, BadRequest
from sqlalchemy.orm.exc import NoResultFound

from app import Application
from app.core.authorization import login_required
from app.core.organizations import update_info
from app.core.common.exceptions import ApplicationException, ValidationException
from app.db.models import DBOrganization
from app.db import DataBase
from swagger_server.models import RequestInfoSetAdmin, ResponseInfo


logger = structlog.get_logger('set_info_endpoint')


@Application.route('/boss_route/info/', methods=['POST'])
@login_required(staff_only=True)
def set_info_endpoint(*args, **kwargs):
    try:
        # TODO: need validation
        request_model = RequestInfoSetAdmin().from_dict(request.json)

        try:
            # TODO: fix it!
            organization: DBOrganization = DataBase.session.query(DBOrganization).one()
            assert organization.id == 1
        except (NoResultFound, AssertionError, AttributeError) as e:
            logger.error(f'Error with organizaion!: {e}')
            raise e

        update_info(organization, request_model.title, request_model.text)

        DataBase.session.commit()

        response = ResponseInfo(
            title=organization.title,
            text=organization.info
        )

        return jsonify(response.to_dict())
    except Exception as e:
        logger.error(f'set_info_endpoint endpoint erorr: {e}')
        raise
