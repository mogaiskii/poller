from flask import request, jsonify
from werkzeug.exceptions import Unauthorized

from app import Application
from app.core.authorization import login_required
from swagger_server.models import Success


@Application.route('/auth/', methods=['GET'])
@login_required
def check_auth_endpoint(*args, **kwargs):
    response = Success(200, 'OK').to_dict()

    return jsonify(response)

