from flask import request, jsonify
from werkzeug.exceptions import Unauthorized
import structlog

from app import Application
from app.core.authorization import authorize, get_auth_token
from swagger_server.models import RequestAuth, ResponseAuth


logger = structlog.get_logger('authorize_endpoint')


@Application.route('/auth/', methods=['POST'])
def authorize_endpoint():
    try:
        request_model = RequestAuth.from_dict(request.json)
        code = request_model.code

        user = authorize(code)

        if user is None:
            raise Unauthorized

        token = get_auth_token(user)

        response = ResponseAuth(token).to_dict()

        return jsonify(response)
    except Exception as e:
        logger.error(f'authorize_endpoint endpoint erorr: {e}')
        raise

