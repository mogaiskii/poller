import datetime

import structlog
from flask import request, jsonify
from werkzeug.exceptions import NotFound, InternalServerError, Forbidden, Gone
from sqlalchemy import func
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.sql import exists

from app import Application
from app.api.helpers.polls import get_user_poll_response
from app.core.authorization import login_required, REQUEST_USER_KWARG
from app.core.common.exceptions import ValidationException
from app.core.polls import get_personized_user_poll_card, validate_poll_user_vote, vote_poll
from app.db.models import DBPollCard, DBUserAllowedVote, DBPollRowVariantUserVote, DBPollRowRangeUserVote
from app.db import DataBase
from swagger_server.models import RequestPollVote, ResponsePollCard, ResponsePollRow, PollRange, ResponsePollVariant


logger = structlog.get_logger('poll_vote_endpoint')


@Application.route('/polls/<poll_id>', methods=['POST'])
@login_required
def poll_vote_endpoint(poll_id, *args, **kwargs):
    try:

        user = kwargs[REQUEST_USER_KWARG]

        request_model = RequestPollVote().from_dict(request.json)

        try:
            poll: DBPollCard = DataBase.session.query(DBPollCard).filter(DBPollCard.id == poll_id).one()
        except NoResultFound:
            raise NotFound

        if not poll.active:
            raise Gone

        if poll.due_to <= datetime.datetime.utcnow():
            raise Gone

        user_allowed_query = DataBase.session.query( DBUserAllowedVote )\
            .filter(DBUserAllowedVote.user_id == user.id)\
            .filter(DBUserAllowedVote.poll_id == poll.id)
        user_allowed = DataBase.session.query(user_allowed_query.exists()).scalar()
        if not user_allowed:
            raise Forbidden

        validate_poll_user_vote(poll, request_model.poll_structure)
        vote_poll(poll, user, request_model.poll_structure)

        DataBase.session.commit()

        poll_model = get_personized_user_poll_card(user)

        poll: DBPollCard = DataBase.session.query(poll_model).filter(poll_model.id == poll_id).one()

        response = get_user_poll_response(poll)

        return jsonify(response.to_dict())
    except Exception as e:
        logger.error(f'poll_vote_endpoint endpoint erorr: {e}')
        raise

