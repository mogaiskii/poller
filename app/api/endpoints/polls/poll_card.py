from flask import request, jsonify
import structlog
from werkzeug.exceptions import NotFound, InternalServerError
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy import func

from app import Application
from app.api.helpers.polls import get_user_poll_response
from app.core.authorization import login_required, REQUEST_USER_KWARG
from app.core.polls import get_personized_user_poll_card
from app.db.models import DBPollCard, DBPollRowVariantUserVote, DBPollRowRangeUserVote
from app.db import DataBase
from swagger_server.models import ResponsePollCard, ResponsePollRow, PollRange, ResponsePollVariant


logger = structlog.get_logger('poll_card_endpoint')


@Application.route('/polls/<poll_id>', methods=['GET'])
@login_required
def poll_card_endpoint(poll_id, *args, **kwargs):
    try:
        user = kwargs[REQUEST_USER_KWARG]

        poll_model = get_personized_user_poll_card(user)

        try:
            poll: DBPollCard = DataBase.session.query(poll_model).filter(poll_model.id == poll_id).one()
        except NoResultFound:
            raise NotFound

        response = get_user_poll_response(poll)

        return jsonify(response.to_dict())
    except Exception as e:
        logger.error(f'poll_card_endpoint endpoint erorr: {e}')
        raise

