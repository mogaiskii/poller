from flask import request, jsonify
import structlog
from werkzeug.exceptions import NotFound, InternalServerError
from sqlalchemy.orm.exc import NoResultFound

from app import Application
from app.core.authorization import login_required
from app.db.models import DBPoll
from app.db import DataBase
from swagger_server.models import ResponsePollsList, ResponsePollBrief


logger = structlog.get_logger('polls_list_endpoint')


@Application.route('/polls/', methods=['GET'])
@login_required
def polls_list_endpoint(*args, **kwargs):
    try:
        limit = request.args.get('limit', 30)
        offset = request.args.get('offset', 0)

        try:
            polls_info_list = DataBase.session.query(DBPoll).limit(limit).offset(offset).all()
        except NoResultFound:
            raise NotFound

        response = ResponsePollsList([
            ResponsePollBrief(
                id=poll.id,
                poll_name=poll.poll_name,
                created_at=poll.created_at,
                due_to=poll.due_to,
                voted_users=poll.voted_users,
                active=poll.active,
                available_votes_actions=poll.available_votes_actions,
                need_attention=poll.active
            ) for poll in polls_info_list
        ])

        return jsonify(response.to_dict())
    except Exception as e:
        logger.error(f'polls_list_endpoint endpoint erorr: {e}')
        raise

