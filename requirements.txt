Flask==1.1.2
flask-marshmallow==0.13.0
Flask-RESTful==0.3.8
Flask-SQLAlchemy==2.4.3
Flask-Migrate==2.5.3
structlog==20.1.0
psycopg2==2.8.5
PyJWT==1.7.1
requests==2.24.0
python-dotenv==0.13.0
Flask-Cors==3.0.8

